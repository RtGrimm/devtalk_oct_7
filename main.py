import numpy as np

xs = np.random.uniform(0, 1, int(1e7))

total = 0
for x in xs:
    total += x

print(total)