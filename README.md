## SOFTWARE ENGINEERING TOOLS FOR SCIENTIFIC COMPUTING

To get started execute the following:
- `git clone https://gitlab.com/RtGrimm/devtalk_oct_7 `
- `cd devtalk_oct_7`
- `git checkout broken`

To view the slides, locate and open `index.html` in `reveal.js-master`. 
