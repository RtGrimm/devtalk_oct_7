import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as la

def LJ_force(x, sigma, epsilon):
    r = la.norm(x, axis=1)
    F = 4 * epsilon * (6*sigma**6/(r**2)**4 - 12*sigma**12/(r**2)**7)

    F = x * np.reshape(F, (F.shape[0], 1))

    F = np.nan_to_num(F, nan=0)

    return F

def compute_forces(xs, sigma, epsilon):
    forces = []

    for x in xs:
        r = xs - x
        force = np.sum(LJ_force(r, sigma, epsilon), axis=0)
        forces.append(force)

    return np.array(forces)

def replay(t_list, x_list, box_size):
    plt.ion()
    plt.figure(figsize=(16, 9))

    for t, xs in list(zip(t_list, x_list))[::10]:
        plt.clf()

        plt.title(f"time:{t}")
        plt.ylim(-box_size, box_size)
        plt.xlim(-box_size, box_size)
        plt.scatter(xs[:, 0], xs[:, 1])

        plt.pause(1e-6)
        plt.draw()


def run_sim():
    particle_count = 30
    scatter_range = 20.0
    delta_t = 0.1
    t_f = 10.0
    t = 0.0

    sigma = 0.24
    epsilon = 0.01
    box_size = 20

    np.random.seed(123)

    x_0 = np.random.uniform(0.0, scatter_range, (particle_count, 3))
    v_0 = np.random.chisquare(6, (particle_count, 3)) * 5e-3

    x_0[:, 2] = 0
    v_0[:, 2] = 0

    F_0 = compute_forces(x_0, sigma, epsilon)
    v_com = np.mean(v_0, axis=0)
    v_0 -= v_com

    x_last = x_0
    x_current = x_0 + v_0 * delta_t + 0.5 * F_0 * delta_t ** 2

    x_list = [x_last, x_current]
    t_list = [0, delta_t]

    while (t < t_f):
        print(f"{t/t_f * 100}%")

        F = compute_forces(x_current, sigma, epsilon)
        x_next = 2 * x_current - x_last + F * delta_t ** 2

        for i in range(0, particle_count):
            if np.any(np.abs(x_next[i, :]) > box_size):
                x_last[i, :] = x_next[i, :]
            else:
                x_last[i, :] = x_current[i, :]
                x_current[i, :] = x_next[i, :]

        t_list.append(t)
        x_list.append(np.copy(x_current))

        t += delta_t

    return x_list, t_list, box_size

def main():
    x_list, t_list, box_size = run_sim()
    replay(t_list, x_list, box_size)


if __name__ == '__main__':
    main()






